import { useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.css';
import Header from './components/Header';
import Details from './page/Details';
import Favorite from './page/Favorite';
import Home from './page/Home';


function App() {
  const [favoriteBook, setfavoriteBook] = useState([]);

  return (
    <Router>
      <div className="App">
        <Header bookCount={favoriteBook.length} />
        <div className="container">

          <Switch>
            <Route exact path="/">
              <Home  favoriteBook={favoriteBook} setfavoriteBook={setfavoriteBook} />
            </Route>

            <Route path="/favorite">
              <Favorite favoriteBook={favoriteBook} setfavoriteBook={setfavoriteBook} />
            </Route>

            <Route path="/details/:id">
              <Details />
            </Route>
          </Switch>

        </div>
      </div>
    </Router>
  );
}

export default App;
