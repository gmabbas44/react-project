import { Link } from "react-router-dom";
import baseUri from "../config/Config";

const Card = ({ book, favorite, setFavorite }) => {
  const addFavoritHandler = (e) => {
    e.preventDefault();

    const alreadyAddedd = favorite.find((fav) => fav.id === book.id);
    const tmpFavorite = [...favorite];

    if (!alreadyAddedd) {
      tmpFavorite.push(book);
      setFavorite(tmpFavorite);
    } else {
      const closeAlert = setTimeout(() => {
        alert("Already Added");
      }, 500);
    }
  };

  const checkFavorite = (id) => {
    const find = favorite.find((book) => book.id === id);
    return find ? true : false;
  };

  return (
    <div className="col-md-4 my-3">
      <Link to={`details/${book.id}`}>
        <div className="card">
          <div className="card-body">
            <img
              src={`${baseUri}${book.image}`}
              alt=""
              className="catd-img-top"
              width="200"
              height="250"
            />

            <h5>{book.bookName}</h5>
            <p>Price : {book.price}</p>
            <button
              onClick={addFavoritHandler}
              className={`btn btn-sm ${
                checkFavorite(book.id) ? "btn-warning" : "btn-primary"
              }`}
              disabled={checkFavorite(book.id)}
            >
              Favorite
            </button>
          </div>
        </div>
      </Link>
    </div>
  );
};

export default Card;
