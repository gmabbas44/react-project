import { Link } from "react-router-dom"

const Header = ({ bookCount }) => {
  return (
    <header className="header sticky-top bg-light">
      <div className="container">
        <nav className="nav">
          <li className="nav-item">
            <Link className="nav-link" to="/">
              Home
            </Link>
          </li>
          <li>
            <Link className="nav-link" to="/favorite">
              Favorite{" "}
              {bookCount === 0 ? (
                ""
              ) : (
                  <span className="badge badge-warning">{bookCount}</span>
                )}
            </Link>
          </li>
        </nav>
      </div>
    </header>
  );
};

export default Header
