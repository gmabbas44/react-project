import baseUri from "../config/Config";

const Favorite = ({ favoriteBook, setfavoriteBook }) => {
  const deleteHendler = (id) => {
    const delData = favoriteBook.filter((book) => book.id !== id)
    setfavoriteBook(delData)
  }

  return (
    <>
      <h2>Favorite Book</h2>
      <table className="table table-bordered table-sm">
        <thead className="table-head">
          <tr>
            <td>id</td>
            <td>Name</td>
            <td>Price</td>
            <td>Image</td>
            <td>Action</td>
          </tr>
        </thead>
        <tbody>
          {favoriteBook.map((book) => (
            <tr>
              <td>{book.id}</td>
              <td>{book.bookName}</td>
              <td>{book.price}</td>
              <td>
                <img
                  width="50"
                  height="60"
                  src={`${baseUri}${book.image}`}
                  alt=""
                />
              </td>
              <td>
                <button
                  onClick={() => deleteHendler(book.id)}
                  className="btn btn-danger btn-sm"
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default Favorite;
