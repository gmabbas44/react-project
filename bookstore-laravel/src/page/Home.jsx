import { useState, useEffect } from "react";
import Card from "../components/Card";

const Home = ({ favoriteBook, setfavoriteBook }) => {
  const [books, setBooks] = useState([]);

  useEffect(() => {
    const uri = "https://bookstore.dev/api/booklist";
    fetch(uri)
      .then((res) => res.json())
      .then((data) => setBooks(data));
  }, []);

  return (
    <>
      <h2>Book List</h2>
      <div className="row">
        {books.map((book) => (
          <Card
            key={book.id}
            book={book}
            favorite={favoriteBook}
            setFavorite={setfavoriteBook}
          />
        ))}
      </div>
    </>
  );
};

export default Home;
