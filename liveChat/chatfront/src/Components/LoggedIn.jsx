import { Link } from "react-router-dom";

const LoggedIn = (props) => {

  const logOutHendler = () => {
    props.setloggedIn(false)
    localStorage.removeItem('username')
    localStorage.removeItem('email')
    localStorage.removeItem('password')
    localStorage.removeItem('image')
  }

  return (
    <div className="flex-row my-3 my-md-0">
      <a className="text-white mr-2 header-search-icon">
        <i className="fas fa-search"></i>
      </a>
      <span className="mr-2 header-chat-icon text-white">
        <i className="fas fa-comment"></i>
        <span className="chat-count-badge text-white"> </span>
      </span>
      <a className="mr-2">
        <img
          className="small-header-avatar"
          // src={`${props.uri}/${localStorage.getItem('image')}`}
          // src="http://127.0.0.1:8000/user/15272.jpg"
        />
      </a>
      <Link to="/create-post" className="btn btn-sm btn-success mr-2">
        Create Post
      </Link>
      <button onClick={ logOutHendler } className="btn btn-sm btn-secondary">Sign Out</button>
    </div>
  );
};

export default LoggedIn;
