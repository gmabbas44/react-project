import { Link } from "react-router-dom";
import LoggedIn from "./LoggedIn";
import LoggOut from "./LoggOut";

const Header = (props) => {
  return (
    <header className="header-bar bg-primary mb-3">
      <div className="container d-flex flex-column flex-md-row align-items-center p-3">
        <h4 className="my-0 mr-md-auto font-weight-normal">
          <Link to="/" className="text-white">
            Live Chat
          </Link>
        </h4>
        {props.loggedIn ? (
          <LoggedIn setloggedIn={props.setloggedIn} uri={props.uri} />
        ) : (
          <LoggOut setloggedIn={props.setloggedIn} />
        )}
      </div>
    </header>
  );
};

export default Header;
