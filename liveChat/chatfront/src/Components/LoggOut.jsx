import axios from "axios";
import { useState } from "react";

const LoggOut = (props) => {
  const [email, setEmail] = useState()
  const [password, setPassword] = useState()

  const handlerSubmit = async (e) => {
    e.preventDefault()
    try {
      const response = await axios.post("/api/login", {
        email,
        password,
      });
      if( response.data ) {
        const userData = response.data
        // console.log( userData );
        props.setloggedIn(true)
        localStorage.setItem('id', userData.id);
        localStorage.setItem('username', userData.username);
        localStorage.setItem('email', userData.email);
        localStorage.setItem('password', userData.password);
        localStorage.setItem('image', userData.image);
      }
      
    } catch (e) {
      console.log( e )
    }
  };
  return (
    <form onSubmit={handlerSubmit} className="mb-0 pt-2 pt-md-0">
      <div className="row align-items-center">
        <div className="col-md mr-0 pr-md-0 mb-3 mb-md-0">
          <input
            onChange={(e) => setEmail(e.target.value)}
            name="email"
            className="form-control form-control-sm input-dark"
            type="text"
            placeholder="Email"
            autoComplete="off"
          />
        </div>
        <div className="col-md mr-0 pr-md-0 mb-3 mb-md-0">
          <input
            onChange={(e) => setPassword(e.target.value)}
            name="password"
            className="form-control form-control-sm input-dark"
            type="password"
            placeholder="Password"
          />
        </div>
        <div className="col-md-auto">
          <button className="btn btn-success btn-sm">Sign In</button>
        </div>
      </div>
    </form>
  );
};

export default LoggOut;
