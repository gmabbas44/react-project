import axios from "axios";
import { useState } from "react";
import { withRouter } from "react-router-dom";
import Container from "../Components/Container";

const Post = (props) => {
  const [title, setTitle] = useState();
  const [body, setBody] = useState();
  const [userId, setUserId] = useState(localStorage.getItem("id"));

  const submitHendler = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post("/api/createPost", {
        title,
        body,
        userId,
      });
      // console.log(response.data);
      props.addFlashMsg(["Congrats! you successfully add a new post!", "success"])
      // props.addFlashMsg("Congrats! you successfully add a new post!")
      props.history.push(`/post/${response.data.id}`)
      
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Container>
      <form onSubmit={submitHendler}>
        <div className="form-group">
          <label htmlFor="post-title" className="text-muted mb-1">
            <small>Title</small>
          </label>
          <input
            onChange={(e) => setTitle(e.target.value)}
            autoFocus
            name="title"
            id="post-title"
            className="form-control form-control-lg form-control-title"
            type="text"
            placeholder=""
            autoComplete="off"
          />
        </div>

        <div className="form-group">
          <label htmlFor="post-body" className="text-muted mb-1 d-block">
            <small>Body Content</small>
          </label>
          <textarea
            onChange={(e) => setBody(e.target.value)}
            name="body"
            id="post-body"
            className="body-content tall-textarea form-control"
            type="text"
          ></textarea>
        </div>

        <button className="btn btn-primary">Save New Post</button>
      </form>
    </Container>
  );
};

export default withRouter(Post);
