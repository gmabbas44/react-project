import axios from "axios";
import { useState } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import FlashMessages from "./Components/FlashMessages";
// import component
import Footer from "./Components/Footer";
import Header from "./Components/Header";
import HomeGuest from "./Components/HomeGuest";
// import pages
import About from "./Pages/About";
import LoggedInHome from "./Pages/LoggedInHome";
import Post from "./Pages/Post";
import Teams from "./Pages/Teams";
import ViewSinglePost from './Pages/ViewSinglePost'

axios.defaults.baseURL = 'http://127.0.0.1:8000'


function App() {
  const public_uri = 'http://127.0.0.1:8000';
  
  const [loggedIn, setloggedIn] = useState(localStorage.getItem("email"));
  const [flashMessages, setFlashMessages] = useState([]);

  const addmsg = (msg) => {
    setFlashMessages( prv => prv.concat(msg) )
  }

  return (
    <Router>
      <FlashMessages messages={flashMessages} />
      <Header loggedIn={loggedIn} setloggedIn={setloggedIn} uri={public_uri} />
      <Switch>
        <Route exact path="/">
          { loggedIn ? <LoggedInHome /> : <HomeGuest /> }
        </Route>
        <Route path="/about-us">
          <About />
        </Route>
        <Route path="/terms">
          <Teams />
        </Route>
        <Route path="/post/:id">
          <ViewSinglePost />
        </Route>
        {loggedIn ? (
          
          <Route path="/create-post">
            <Post addFlashMsg={addmsg}/>
          </Route>
          
        ) : (
          <HomeGuest />
        )}
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
