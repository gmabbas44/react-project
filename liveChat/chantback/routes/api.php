<?php

use App\Http\Controllers\ChatUserController;
use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('reactUsers', [ChatUserController::class, 'getUsers']);
Route::post('addUser', [ChatUserController::class, 'addUser']);
Route::post('login', [ChatUserController::class, 'login']);

Route::post('createPost', [PostController::class, 'createPost']);