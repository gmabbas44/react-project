<?php

namespace App\Http\Controllers;

use App\Models\ChatUser;
use Illuminate\Http\Request;

class ChatUserController extends Controller
{
    public function getUsers()
    {
        $all = ChatUser::all();
        return response()->json($all);
    }

    public function addUser(Request $request, ChatUser $user) 
    {
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();
        return response()->json($user);
    }

    public function login(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        $user = ChatUser::where([
            ['email', '=', $email],
            ['password', '=', $password]
        ])->first();
        if( $user ) {
            $response = response()->json($user);
        } else {
            $response = json_encode(['status' => false, 'data' => 'Not Found']);
        }
        return $response;

    }
}
