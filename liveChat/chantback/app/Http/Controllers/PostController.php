<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function createPost(Request $req, Post $post)
    {
        $req->validate([
            'title' => 'required',
            'body' => 'required',
            'userId' => 'required'
        ]);
        $post->title = $req->title;
        $post->body = $req->body;
        $post->chat_user_id = $req->userId;
        $post->save();
        if( $post ) {
            return response()->json($post);
        } else {
            return response()->json(['status'=>'failed', 'post' => 'Post added not successfully!']);
        }
    }
}
