<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ChatUser as Users;

class ChatUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Users::insert([
            'username' => 'Gm Abbas Uddin',
            'email' => 'gmabbas44@gmail.com',
            'password' => '123456',
            'image' => '',
            'token' => ''
        ]);
    }
}
