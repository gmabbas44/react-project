import { useEffect, useState } from 'react';
import { useParams } from "react-router-dom";
import CardBtn from "./CardBtn";


const Details = ({ isLoading }) => {

    const [product, setProduct] = useState([]);
    const { id } = useParams();
    useEffect(() => {
        fetch(`https://fakestoreapi.com/products/${id}`)
            .then(res => res.json())
            .then(data => setProduct(data));

    }, [id]);
    // console.log(product);

    // console.log( 'details', typeof id )

    // const productData = products?.find(product => product.id === parseInt(id));

    return (
        <div className="row mx-auto justify-content-center" >
            <div className="col-md-6">
                {isLoading && 'Loading....'}
                {product && (
                    <div className="card">
                        <div className="card-body">
                            <img src={product.image} width="500" height="400" className="catd-img-top" alt="Product Image" />
                            <h5>{product.title}</h5>
                            <p>{product.description}</p>
                            <p>Price : {product.price}</p>
                            <CardBtn product={product} />
                        </div>
                    </div>
                )}
            </div>

        </div>
    )
}

export default Details;