import { Link } from "react-router-dom";
import CardBtn from "./CardBtn";


export default function Card({ product }) {
    // console.log( product )

    return (
        <div key={product.id} className="col-md-4 my-3" >
            <div className="card">
                <div className="card-body">
                    <Link to={`details/${product.id}`}>
                        <img src={product.image} alt="" className="catd-img-top" width='200' height='250' />
                    </Link>
                    <h5>{product.title}</h5>
                    <p>Price : {product.price}</p>
                    <CardBtn product={product} />
                </div>
            </div>
        </div>
    )
}
