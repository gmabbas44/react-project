import React from 'react';


const CardBtn = ({ product }) => {

  const addToCart = () => {
    let data = {
      id: product.id,
      title: product.title,
      image: product.image,
      price: product.price
    }

    let cart = JSON.parse(localStorage.getItem('cartData')) === null
      ? []
      : JSON.parse(localStorage.getItem('cartData'));

    const newCart = [...cart, data]
    localStorage.setItem('cartData', JSON.stringify(newCart));
    alert('Added Successfully!')

  }

  return (
 
    <button onClick={addToCart} className="btn btn-primary">Add To Card</button>

  )
}

export default CardBtn;