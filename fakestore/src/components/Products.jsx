import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router,
  Switch,
  Route,
  Link } from 'react-router-dom';
import Card from './Card';
import Details from './Details';

const Products = ({products, setProducts , setCardProduct}) => {

  return (
    <div>
      <h1>Products List</h1>
      <div className="row">
        {
          products.map(product => (
            <Card product={product}/>
          ))
        }
        
      </div>
    </div>
  )
}

export default Products;