import { useState } from "react";
const ProductCard = () => {

  const data = JSON.parse(localStorage.getItem('cartData'));
  console.log( data.length );

  const [cartData, setCartData] = useState(data);
  

  const deleteItem = (id) => {
    // const findData = cartData.find( product => product.id === id );
    // const findData = cartData.findIndex(product => product.id === id);
    const updataData = cartData.filter(product => product.id !== id);
    localStorage.setItem('cartData', JSON.stringify(updataData))
    setCartData(updataData)
  }

  return (
    <div className="table">
      <h3>Cart Product</h3>
      <table className="table">
        <thead>
          <tr>
            <th>SL</th>
            <th>Image</th>
            <th>Title</th>
            <th>Price</th>
            <th>Remove</th>
          </tr>
        </thead>
        <tbody>
          {
            cartData.map((product) => (
              <tr key={product.id}>
                <td>{product.id}</td>
                <td><img src={product.image} alt="" width='50'/></td>
                <td>{product.title}</td>
                <td>{product.price}</td>
                <td><button onClick={(() => deleteItem(product.id))} className="btn btn-danger btn-sm">Delete</button></td>
              </tr>

            ))
          }

        </tbody>
      </table>
    </div>


  )
}

export default ProductCard;