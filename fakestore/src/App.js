import { useEffect, useState } from 'react';
import { Route, BrowserRouter as Router, Switch, Link } from 'react-router-dom';
import './App.css';
import Details from './components/Details';
import Products from './components/Products'
import ProductCard from './pages/ProductCart'

function App() {

  const [products, setProducts] = useState([]);
  const [isLoading, setisLoading] = useState(false);
  useEffect(() => {
    setisLoading(true)
    fetch('https://fakestoreapi.com/products')
      .then(res => res.json())
      .then(data => {
        setProducts(data)
        setisLoading(false)
      });
  }, [])
  return (
    <Router basename="/">
      <div className="App">
        <div className="container">
          <ul className="nav">
            <li className="nav-item">
              <Link className="nav-link" to='/'>Home</Link>
            </li>
            <li className="nav-item">
              {/* <a className="nav-link active" href="#">Active</a> */}
              <Link className="nav-link" to="/card">Card</Link>
            </li>
          </ul>
          <Switch>
            <Route exact path='/'>
              <Products products={products} setProducts={setProducts} />
            </Route>
            <Route path='/details/:id'>
              <Details isLoading={isLoading} />
            </Route>
            <Route path='/card'>
              <ProductCard  />
            </Route>
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
